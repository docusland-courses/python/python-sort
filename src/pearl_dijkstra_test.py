from pearl_dijkstra import dijkstra_pearl

import unittest
class PearlTest(unittest.TestCase):
    """
    Coding exercise for IT students.
    The target of this script is to store different algorithms.
    """

    def test_pearl_dijkstra(self):
        self.assertEqual("0102012021012010201", dijkstra_pearl(iterations=20))
    def test_pearl_dijkstra(self):
        self.assertEqual("010201202101201020120210201021012010201202101201", dijkstra_pearl(iterations=50))
