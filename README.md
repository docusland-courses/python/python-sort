Exercice
=

This project is meant as an exercice for IT students.

Sorting arrays
==
Sorting an array is a common exercise.  Even though a developper no longer needs to sort arrays as many methods already exist. 
It is a very good practice to train on implementing them. 

On one side, you shall train your brain to deal with some coding subtilities. 
But also, on another side, these algorythms are used by companies as 'hiring' exercices. So you'd better know how they work!
- bubble sort
- selection sort
- quick sort
- heap sort
- merge sort
- insertion sort

Backtrack
==
Another algorythm that is nice to discover is backtracking. 
It is the concept of try and error, and stepping backwards in order to try another solution. 
Here we have an exercise linked to this concept : 'Les perles de Dijkstra' 

Installation
==
How to install it ? Nothing easier. 
First, you have to have python  installed on your computer. 

Then launch : `pip install -r requirements.txt`.

Explanation
==

Sorting arrays
===

This project is implemented the following way :
 - **src/sort_test.py** : Tests written. You should not modify these tests. 
 - **src/sort.py** : The file you have to produce. For each method, a docstring is written in order to explain the logic required. 

Backtracking
===
This project is implemented the following way :
 - **src/pearl_dijkstra_test.py** : Tests written. You should not modify these tests. 
 - **src/pearl_dijkstra.py** : The file you have to produce. For each method, a docstring is written in order to explain the logic required. 

 Running the tests
 ==
 
 There are several ways for executing your tests :
  - **With pycharm**, you can simply open the test file and click on the green arrow button shown on the left side of each test.
  - **With your terminal**, you can type `cd src/;pytest -v`

Going further
==
Have you ever heard about PEP ? This coding standard on coding best practices and namings? 
Use the linter and improve your coding skills by calling the script: 

`pylint src/*.py` 
